

const BtnMenu =({ data })=>{
    console.log(data)
    return(
        <div className="flex flex-col items-center">
          <button className="w-16 h-16 bg-primary3 rounded shadow-md flex justify-center items-center">
            <img className="o" src={data.img} alt="Btn-Icon"/>
          </button>
          <span className="block text-xs text-Grey-1 font-bold mt-2 w-full text-center">{data.txt_menu}</span>
        </div>
    )
}

export default BtnMenu