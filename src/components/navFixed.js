import homeIcon from '../assets/home-icon.svg'
import userIcon from '../assets/user-icon.svg'

const NavFixed =()=>{
    return(
        <div className="fixed bottom-0 max-w-md sm:px-8 left-0 right-0 mx-auto flex justify-around items-center h-20">
            <button className="flex flex-col items-center">
                <img className="w-5" src={homeIcon} alt="aa"/>
                <span className="inline-block text-sm mt-1 text-PrimaryColor">Beranda</span>
            </button>
            <button className="flex flex-col items-center">
                <img className="w-5" src={userIcon} alt="aa"/>
                <span className="inline-block text-sm mt-1 text-Grey-4">Akun</span>
            </button>
        </div>
    )
}

export default NavFixed