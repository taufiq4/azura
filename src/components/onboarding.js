import { useEffect, useState } from "react"
import { useHistory, Link } from "react-router-dom"

import illus1 from '../assets/onboarding/Ilustrasi 1.png'
import illus2 from '../assets/onboarding/Ilustrasi 2.png';
import illus3 from '../assets/onboarding/Ilustrasi 3.png';

const dataSlide = [
    {
        img: illus1,
        title: 'Data Diri Karyawan',
        desc: 'Kini Anda lebih mudah dalam mengatur data diri melalui aplikasi'
    },
    {
        img: illus2,
        title: 'Pengajuan Data',
        desc: 'Mengajukan Perubahan Data Diri, Data Keluarga, dan Surat Ijin Layanan'
    },
    {
        img: illus3,
        title: 'Payroll',
        desc: 'Anda dapat mengecek dan mengunduh slip gaji Anda melalui aplikasi'
    },
]

const OnBoarding =()=>{
    const [slide, setSlide] = useState(1)

    const history = useHistory()

    const handleSlide =()=>{
        setSlide(curr => curr += 1)
        if(slide > 2){
            history.push('/login')
        }
    }

    useEffect(()=>{
        console.log(slide)
    }, [slide])
    
    return(
        <div className="h-full relative">
            <div className="h-full px-3 flex flex-col justify-center">
                <img className="block mx-auto mb-12" src={dataSlide[slide - 1].img} alt="hi"/>
                <div className="w-full text-center">
                    <h3 className="text-xl text-Grey-1">{dataSlide[slide - 1].title}</h3>
                    <p className="text-sm text-Grey-1 mt-3 max-w-sm mx-auto">{dataSlide[slide - 1].desc}</p>
                </div>
            </div>
            <div className="w-full h-16 flex justify-between px-3 bottom-0 absolute"> 
                <button className="text-Secondary">
                    <Link to="/login">Lewati</Link>
                </button>
                <div className="h-full flex items-center">
                    <span className={`${slide === 1 ? `w-4 bg-primary` : `w-2 bg-secondary`} h-2 inline-block rounded-full mx-2`}></span>
                    <span className={`${slide === 2 ? `w-4 bg-primary` : `w-2 bg-secondary`} h-2 inline-block rounded-full mx-2`}></span>
                    <span className={`${slide === 3 ? `w-4 bg-primary` : `w-2 bg-secondary`} h-2 inline-block rounded-full mx-2`}></span>
                </div>
                <button className="text-PrimaryColor" onClick={()=> handleSlide()}>{slide === 3 ? `Login` : `Lanjut`}</button>
            </div>
        </div>
    )
}

export default OnBoarding