import { useEffect, useState } from 'react'
import { KelolaData, Absensi } from '../utils/dataButtonMenu'
import HomeSkeleton from './homeSkeleton'
import BtnMenu from './btnMenu'

function MenuHome(){
    const [skeleton, setSkeleton] =  useState(true)
    useEffect(()=>{
        const timeSkeleton = setTimeout(()=>{
            setSkeleton(false)
        }, 1500)
        return ()=> clearTimeout(timeSkeleton)
    })
    return(
        <div className="px-4 py-3">
           {skeleton ? <HomeSkeleton/> : (
            <>
                <h2 className="font-bold text-sm mt-2 text-Grey-1">Kelola Data</h2>
                <div className="w-full grid grid-cols-4 mt-5">
                    {
                        KelolaData.map(data => (<BtnMenu key={data.id} data={data}/>))
                    }
                </div>                  
                <h2 className="font-bold text-sm mt-10 text-Grey-1">Absensi</h2>
                <div className="w-full grid grid-cols-4 mt-5">
                    {
                        Absensi.map(data => (<BtnMenu key={data.id} data={data}/>))
                    }
                </div>   
            </>    
           )} 
        </div>
    )
}

export default MenuHome