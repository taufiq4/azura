 import Skeleton from 'react-loading-skeleton'

 const HomeSkeleton =()=>{
     return(
         <>
            <Skeleton height={25} width={80}/>
            <div className="w-full grid grid-cols-4 mt-4">
                <div className="flex flex-col items-center">
                  <Skeleton height={70} width={70}/>
                  <Skeleton className="mt-1.5" height={10} width={40}/>
                </div>
                <div className="flex flex-col items-center">
                  <Skeleton height={70} width={70}/>
                  <Skeleton className="mt-1.5" height={10} width={40}/>
                </div>
                <div className="flex flex-col items-center">
                  <Skeleton height={70} width={70}/>
                  <Skeleton className="mt-1" height={10} width={40}/>
                </div>
                <div className="flex flex-col items-center">
                  <Skeleton height={70} width={70}/>
                  <Skeleton className="mt-1.5" height={10} width={40}/>
                </div>
            </div>
            <div className="w-full mt-2 flex justify-center">
              <Skeleton className="mx-auto" height={200} width={320}/>                
            </div>
         </>
     )
 }

 export default HomeSkeleton