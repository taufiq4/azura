import logoSplash from '../assets/logo-splash-screen.png'
import logoRSISA from '../assets/logo-rsisa.png'

const SplashScreen=()=>{
    return (
        <div className="h-full w-full relative">
            <div className="h-full bg-white flex justify-center items-center">
              <img className="w-28 mb-10" src={logoSplash} alt="logo-splash"/>
            </div>
            <div className="absolute bottom-0 w-full flex justify-center">
                <img className="inline-block w-32 my-8" src={logoRSISA} alt="logo-RSISA"/>
            </div>
        </div>
    )
}

export default SplashScreen