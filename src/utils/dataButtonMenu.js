// Kelola Data
import DataDiri from '../assets/icon-menu/Data Diri.svg'
import Keluarga from '../assets/icon-menu/Keluarga.svg'
import JabatanStruktural from '../assets/icon-menu/Jabatan Struktural.svg'
import Profesi from '../assets/icon-menu/Profesi.svg'

//Absensi
import Sakit from '../assets/icon-menu/Sakit.svg'
import Ijin from '../assets/icon-menu/Ijin.svg'
import Cuti from '../assets/icon-menu/Cuti.svg'
import Shift from '../assets/icon-menu/Shift.svg'

export const KelolaData = [
    {
        id: 1,
        txt_menu: 'Data Diri',
        img: DataDiri
    },
    {
        id: 2,
        txt_menu: 'Keluarga',
        img: Keluarga
    },
    {
        id: 3,
        txt_menu: 'Jabatan Struktural',
        img: JabatanStruktural
    },
    {
        id: 4,
        txt_menu: 'Profesi',
        img: Profesi
    }    
]

export const Absensi = [
    {
        id: 1,
        txt_menu: 'Sakit',
        img: Sakit
    },
    {
        id: 2,
        txt_menu: 'Ijin',
        img: Ijin
    },
    {
        id: 3,
        txt_menu: 'Cuti',
        img: Cuti
    },
    {
        id: 4,
        txt_menu: 'Shift',
        img: Shift
    }    
]