// import { useState } from 'react'
import { BrowserRouter as Router, Route, Switch,  } from 'react-router-dom'
import { Auth } from '../context/authContext'
import Home from './Home'
import Login from './login'

function App() {

  return (
    <div className="h-screen font-mulish max-w-md mx-auto sm:px-8">
     <Auth>
      <Router>
        <Switch>
          <Route exact path="/">
            <Home/>
          </Route>
          <Route path="/login">
            <Login/>
          </Route>
          <Route path="/forgotpassword">
            <h2>Forgot Password</h2>
          </Route>
        </Switch>
      </Router>     
     </Auth>      
    </div>  
  );
}

export default App;
