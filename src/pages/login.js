import { useState, useContext } from 'react'
import { useForm } from 'react-hook-form' 
import { useHistory, Link } from 'react-router-dom'
import { authContext } from '../context/authContext'
import logoSplash from '../assets/logo-splash-screen.png'
import closeEye from '../assets/Invisible.svg'
import openEye from '../assets/open eye.svg'
import info from '../assets/info.svg'

function Login() {
    const { handleLogin } = useContext(authContext)
    const { register, formState: { errors }, handleSubmit } = useForm()
    const history = useHistory()
    const [ ShowPassword, setShowPassword ] = useState(false)
    const [ errorTxt, setErrorTxt ] = useState('')

    const onSubmits = async(data) =>{
        const resData = await handleLogin(data)
        if(resData.error){
            if(resData.message === "password tidak sesuai"){
                setErrorTxt('Kata sandi salah. Coba lagi atau klik lupa kata sandi untuk menyetel ulang sandi.')
            } else{
                setErrorTxt(resData.message)
            }
            
        } else{
            setErrorTxt('')
            history.push('/')
        }
    }

    // useEffect(() => {

    // }, [dataAuth])

    return(
        <div className="h-full w-full px-3 flex flex-col items-center justify-center">
            <img className="block w-28 mb-1" src={logoSplash}/>
            <form className="w-full" onSubmit={handleSubmit(onSubmits)}>
                <div className="h-14 w-full px-3 my-2">
                    {
                      errorTxt ? 
                        (<div className="w-full flex items-center h-full bg-red px-2 rounded-md">
                            <img src={info} alt="jiji"/>
                            <p className="text-sm text-White font-light ml-2">
                              {errorTxt}
                            </p>
                        </div>) : ''
                    }
                </div>
                <div className="flex flex-col max-w-xs mx-auto">
                    <h2 className="text-Secondary text-center mb-2 text-xl text-Grey-1">Selamat Datang</h2>
                    <div className="border-2 w-full text-sm my-2 p-1 rounded-md">
                        <input {...register("nip", { required: true })} className="p-2 w-full outline-none text-Secondary" type="text" placeholder="NIP"/>
                    </div>
                    <div className="border-2 w-full text-sm my-2 p-1 rounded-md relative">
                        <input 
                          {...register("password", { required: true })} 
                          className="p-2 outline-none w-full text-Secondary" 
                          type={ShowPassword ? "text" : "password"} 
                          placeholder="Kata Sandi"
                        />
                        <i className={`absolute right-3 ${ShowPassword ? 'top-4' : 'top-3'}`} onClick={()=> setShowPassword(!ShowPassword)}><img src={ShowPassword ? openEye : closeEye}/></i>
                    </div>
                    <div className="flex justify-end mb-2">
                        <Link className="text-sm text-PrimaryColor" to="/forgotpassword">lupa kata sandi ?</Link>
                    </div>
                    <button className="w-full bg-primary text-White rounded-md  p-3" type="submit">Masuk</button>
                    <div className="my-3 flex justify-around items-center w-full">
                        <span className="inline-block bg-secondary mx-2 w-full h-0.5"></span>
                        <span className="inline-block text-md text-Secondary">atau</span>
                        <span className="inline-block bg-secondary mx-2 w-full h-0.5"></span>
                    </div>
                </div>
            </form>
            <div className="w-80 mx-auto">
                <button className="w-full bg-white text-Secondary rounded-md mt-1 p-3 shadow-md">Masuk dengan nomor telepon</button>
            </div>
        </div>
    )
}

export default Login