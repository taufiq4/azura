import { useContext } from 'react'
import { authContext } from '../context/authContext'

//component
import SplashScreen from '../components/splashScreen'
import OnBoarding from '../components/onboarding'
import NavFixed from '../components/navFixed'
import MenuHome from '../components/menuHome'

//image
import logohome from '../assets/Logo home.png'
import notif from '../assets/Notif.svg'

function Home() {
    const { dataAuth, splash } = useContext(authContext)
    console.log(dataAuth)
    return(
        <div className="h-full relative">
            {splash ? <SplashScreen/> : !dataAuth ? (<OnBoarding/>) : (
                <>
                <div className="h-36 flex flex-col justify-between py-3 px-5 w-full bg-primary2">
                    <div className="flex justify-between">
                        <img className="block w-44" src={logohome} alt="logo"/>
                        <img className="block w-5" src={notif} alt="notification"/>
                    </div>

                    <div>
                        <h2 className="text-White">Hai, {dataAuth.nama}</h2>
                        <p className="text-xs text-White">Jangan lupa berdoa sebelum bekerja :)</p>
                    </div>
                </div> 
                <MenuHome/>
                <NavFixed/>    
                </>    
            )}
        </div>
    )
}

export default Home