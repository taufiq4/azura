import { createContext, useEffect, useState } from "react";

const Endpoint = "https://hr-stag.dev.azuralabs.id/api"

export const authContext = createContext()

export function Auth({ children }) {
    const [dataAuth, setDataAuth] = useState()
    const [splash, setSplash] = useState(true)

    const handleLogin = async(data)=>{
        try {
            const CekTokenToApi = await fetch(`${Endpoint}/auth/login`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json; charset=UTF-8'
                }
            })
            const resData = await CekTokenToApi.json()
            
            if(resData.error){
                return resData
            } else{
                setDataAuth(resData.data.user)
                return resData
            }
        } catch (err) {
            console.log(err)
        }

    }
    
    useEffect(()=>{
        setTimeout(()=>{
            setSplash(false)
        }, 1200)
    }, [])

    return ( 
        <authContext.Provider value={{ dataAuth, splash, handleLogin }}>
            { children }
        </authContext.Provider>
    )
}

